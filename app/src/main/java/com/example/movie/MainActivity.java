package com.example.movie;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.movie.view.adapter.MyViewPageAdapter;
import com.example.movie.view.transformer.DepthPageTransformer;
import com.example.movie.view.transformer.ZoomOutPageTransformer;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity  {
    private BottomNavigationView mBottomNavigationView;
    private ViewPager2 mViewPager2;
    private MyViewPageAdapter mMyViewPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager2 = findViewById(R.id.view_pager);
        mBottomNavigationView = findViewById(R.id.bottom_nav);
        mMyViewPageAdapter = new MyViewPageAdapter(this);
        mViewPager2.setAdapter(mMyViewPageAdapter);
        mViewPager2.setUserInputEnabled(false);



        mBottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id ==R.id.action_home){
                    mViewPager2.setCurrentItem(0);
                }else if (id == R.id.action_hot){
                    mViewPager2.setCurrentItem(1);
                }else if (id == R.id.action_setting){
                    mViewPager2.setCurrentItem(2);
                }
                return true;
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        mViewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position){
                    case 0:
                        mBottomNavigationView.getMenu().findItem(R.id.action_home).setChecked(true);
                        break;
                    case 1:
                        mBottomNavigationView.getMenu().findItem(R.id.action_hot).setChecked(true);
                        break;
                    case 2:
                        mBottomNavigationView.getMenu().findItem(R.id.action_setting).setChecked(true);
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_zoom){
            mViewPager2.setPageTransformer(new ZoomOutPageTransformer());
        }else if (id == R.id.menu_depth){
            mViewPager2.setPageTransformer(new DepthPageTransformer());
        }
        return true ;
    }


}