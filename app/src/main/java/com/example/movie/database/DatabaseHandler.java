package com.example.movie.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.movie.model.Notification;
import com.example.movie.model.Watched;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME ="MOVIE DATABASE";

    //table notification
    private static final String TABLE_NOTIFICATION = "Notifications";
    private static final String KEY_ID = "Id";
    private static final String KEY_TITLE = "Title";
    private static final String KEY_DESCRIPTION = "Description";
    private static final String KEY_STATUS = "Status";
    private static final String KEY_DATE = "Date";

    //table movie Watched
    private static final String TABLE_WATCHED = "Watched";
    private static final String ID = "Id";
    private static final String CATAGORY_ID = "CategoryId";
    private static final String STAR = "Star";
    private static final String IMDB_PO_INTEGER= "ImdbPoInteger";
    private static final String NAME = "Name";
    private static final String IS_ACTIVE = "IsActive";
    private static final String IS_HOT = "IsHot";
    private static final String AVATAR = "Avatar";
    private static final String VIEW_NUMBER = "ViewNumber";
    private static final String LINK = "Link";
    private static final String DESCRIPTION = "Description";
    private static final String POSTER = "Poster";
    private static final String QUALITY = "Quality";
    private static final String DURATION = "Duration";
    private static final String DATE = "Date";
    private static final String REDIRECT_LINK = "RedirectLink";

    private Context context;

    public DatabaseHandler(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table notificatio

        String createTableNotification = "create table " + TABLE_NOTIFICATION + "(" + KEY_ID + " integer primary key,"
                + KEY_TITLE + " text," + KEY_DESCRIPTION +" text," +KEY_STATUS +" integer,"+KEY_DATE +" text" +")";

        db.execSQL(createTableNotification);


        //create table watcher

        String createTableWatcher = "create table " + TABLE_WATCHED + "(" + ID + " integer primary key," +NAME +" text unique," + AVATAR + " text,"
                + LINK + " text,"+VIEW_NUMBER+" integer,"  + DATE +" text," +REDIRECT_LINK+ " text" + ")";
        db.execSQL(createTableWatcher);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WATCHED);

        onCreate(db);
    }

    public List<Notification> getListNotification(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Notification> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NOTIFICATION, null);
        if (cursor.moveToFirst()){
            do {
                Notification notification = new Notification();
                notification.setId(cursor.getInt(0));
                notification.setTitle(cursor.getString(1));
                notification.setDescription(cursor.getString(2));
                notification.setStatus(cursor.getInt(3));
                notification.setDate(LocalDateTime.parse(cursor.getString(4)));
                list.add(notification);

            }while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return list;
    }

    public List<Watched> getListWatched(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Watched> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + TABLE_WATCHED, null);
        if (cursor.moveToFirst()){
            do {
                Watched watched = new Watched();
                watched.setId(cursor.getInt(0));
                watched.setName(cursor.getString(1));
                watched.setAvatar(cursor.getString(2));
                watched.setLink(cursor.getString(3));
                watched.setViewNumber(cursor.getInt(4));
                watched.setDate(LocalDateTime.parse(cursor.getString(5)));
                watched.setRedirectLink(cursor.getString(6));
                list.add(watched);
            }while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return  list;
    }

    public void addNotification(Notification notification){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE,notification.getTitle());
        values.put(KEY_DESCRIPTION,notification.getDescription());
        values.put(KEY_STATUS,0);
        values.put(KEY_DATE,String.valueOf(notification.getDate()));
        db.insert(TABLE_NOTIFICATION,null,values);
        db.close();

    }

    public void addWatched(Watched watched){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME,watched.getName());
        values.put(AVATAR,watched.getAvatar());
        values.put(LINK,watched.getLink());
        values.put(VIEW_NUMBER, watched.getViewNumber());
        values.put(DATE,String.valueOf(watched.getDate()));
        values.put(REDIRECT_LINK,watched.getRedirectLink());
        db.insert(TABLE_WATCHED,null,values);
        db.close();

    }

    public int deleteNotification(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(TABLE_NOTIFICATION,KEY_ID +"=?",new String[]{String.valueOf(id)});
        db.close();
        return result;
    }

    public int deleteWatched(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        int ab = db.delete(TABLE_WATCHED,ID+"=?",new String[]{String.valueOf(id)});
        db.close();
        return ab;
    }

    public void updateNotification(Notification notification){
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DESCRIPTION,notification.getDescription());
        values.put(KEY_STATUS,notification.getStatus());
        db.update(TABLE_NOTIFICATION,values,KEY_ID+"=?",new String[]{String.valueOf(notification.getId())});
        db.close();
    }

    public void updateWatched(Watched watched){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME,watched.getName());
        values.put(AVATAR,watched.getAvatar());
        values.put(LINK,watched.getLink());
        values.put(VIEW_NUMBER, watched.getViewNumber());
        values.put(DATE,String.valueOf(watched.getDate()));
        db.update(TABLE_WATCHED,values,NAME + "=?", new String[]{String.valueOf(watched.getName())});
    }


    public Watched getNameWatched(String name){
        Watched watched = new Watched();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_WATCHED,new String[]{ID, NAME,AVATAR,LINK,VIEW_NUMBER,DATE,REDIRECT_LINK},NAME + "=?",new String[]{name},null,null,null);
        if (cursor.getCount() >0 && cursor !=null){
            if(cursor.moveToFirst()){
                do {
                    watched.setId(cursor.getInt(0));
                    watched.setName(cursor.getString(1));
                    watched.setAvatar(cursor.getString(2));
                    watched.setLink(cursor.getString(3));
                    watched.setViewNumber(cursor.getInt(4));
                    watched.setDate(LocalDateTime.parse(cursor.getString(5)));
                    watched.setRedirectLink(cursor.getString(6));
                    cursor.close();
                    return watched;
                }while (cursor.moveToNext());
            }

        }else {
            return null;
        }
        return watched;
    }


}
