package com.example.movie.api;

import com.example.movie.model.Data;
import com.example.movie.model.DetailFilm;
import com.example.movie.model.FilmCatagoryList;
import com.example.movie.model.Films;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ApiService {
    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create();

    ApiService apiService = new Retrofit.Builder()
            .baseUrl("http://cinema.tl/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(ApiService.class);

    //get-home-film
    @GET("api/v1/get-home-films")
    Call<Data> getHomeMovie(@Query("msisdn") String msisdn,@Header("wsToken") String wsToken);

    //get-catagory-list
    @GET("api/v1/get-film-category-list")
    Call<FilmCatagoryList> getFilmCatagoryList(@Query("wsToken") String wsToken, @Header("wsToken") String wsToken1);

    //get-film-detail
    @GET("api/v1/get-film-detail")
    Call<DetailFilm> getFilmDetail(@Query("filmId") int filmId, @Header("wsToken") String wsToken);

    //get-film-by-catagory
    @GET("api/v1/get-films-by-category")
    Call<Data> getMoreLike(@Query("categoryId") int categoryId, @Query("page") int page, @Query("size") int size,@Header("wsToken") String wsToken);

    //get-relate-film
    @GET("api/v1/get-related-film")
    Call<Data> getRelatedFilm(@Query("categoryId") int categoryId, @Query("page") int page, @Query("size") int size,@Header("wsToken") String wsToken);

    //search-film
    @GET("api/v1/search-films")
    Call<Data> searchFilm(@Query("keySearch") String keySearch,@Header("wsToken") String wsToken);
}
