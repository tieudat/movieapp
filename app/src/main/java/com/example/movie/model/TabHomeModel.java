package com.example.movie.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TabHomeModel implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("position")
    private String position;
    @SerializedName("type")
    private String type;
    @SerializedName("layout")
    private String layout;
    @SerializedName("listPhoto")
    private List<Photo> listPhoto;
    @SerializedName("listFilm")
    private List<Films> listFilm;
    @SerializedName("listCatagory")
    private List<Catagory> listCatagory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public List<Photo> getListPhoto() {

        if (listPhoto == null){
            listPhoto = new ArrayList<>();
        }
        return listPhoto;
    }

    public void setListPhoto(List<Photo> listPhoto) {
        this.listPhoto = listPhoto;
    }

    public List<Films> getListFilm() {
        if (listFilm ==null){
            listFilm = new ArrayList<>();
        }
        return listFilm;
    }

    public void setListFilm(List<Films> listFilm) {
        this.listFilm = listFilm;
    }

    public List<Catagory> getListCatagory() {
        if (listCatagory == null){
            listCatagory = new ArrayList<>();
        }
        return listCatagory;
    }

    public void setListCatagory(List<Catagory> listCatagory) {
        this.listCatagory = listCatagory;
    }
}
