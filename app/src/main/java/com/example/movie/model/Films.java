package com.example.movie.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Films implements Serializable {
    private int id;
    private int categoryId;
    private int star;
    private int imdbPoInteger;
    private String name;
    private int isActive;
    private int isHot;
    private String avatar;
    private int viewNumber;
    private String link;
    private String description;
    private String poster;
    private String quality;
    private int episodesTotal;
    @SerializedName("subVideoList")
    private List<SubVideo> listSubVideo;
    private String redirectLink;



    public Films() {
    }

    public Films(int id, int categoryId, int star, int imdbPoInteger, String name, int isActive, int isHot, String avatar, int viewNumber, String link, String description, String poster, String quality, int episodesTotal, List<SubVideo> listSubVideo, String redirectLink) {
        this.id = id;
        this.categoryId = categoryId;
        this.star = star;
        this.imdbPoInteger = imdbPoInteger;
        this.name = name;
        this.isActive = isActive;
        this.isHot = isHot;
        this.avatar = avatar;
        this.viewNumber = viewNumber;
        this.link = link;
        this.description = description;
        this.poster = poster;
        this.quality = quality;
        this.episodesTotal = episodesTotal;
        this.listSubVideo = listSubVideo;
        this.redirectLink = redirectLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getImdbPoInteger() {
        return imdbPoInteger;
    }

    public void setImdbPoInteger(int imdbPoInteger) {
        this.imdbPoInteger = imdbPoInteger;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getViewNumber() {
        return viewNumber;
    }

    public void setViewNumber(int viewNumber) {
        this.viewNumber = viewNumber;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public int getEpisodesTotal() {
        return episodesTotal;
    }

    public void setEpisodesTotal(int episodesTotal) {
        this.episodesTotal = episodesTotal;
    }

    public List<SubVideo> getListSubVideo() {
        return listSubVideo;
    }

    public void setListSubVideo(List<SubVideo> listSubVideo) {
        this.listSubVideo = listSubVideo;
    }

    public String getRedirectLink() {
        return redirectLink;
    }

    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }

    @Override
    public String toString() {
        return "Films{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", star=" + star +
                ", imdbPoInteger=" + imdbPoInteger +
                ", name='" + name + '\'' +
                ", isActive=" + isActive +
                ", isHot=" + isHot +
                ", avatar='" + avatar + '\'' +
                ", viewNumber=" + viewNumber +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", poster='" + poster + '\'' +
                ", quality='" + quality + '\'' +
                ", episodesTotal=" + episodesTotal +
                ", listSubVideo=" + listSubVideo +
                ", redirectLink='" + redirectLink + '\'' +
                '}';
    }
}
