package com.example.movie.model;

import com.google.gson.annotations.SerializedName;

public class DetailFilm {
    @SerializedName("data")
    private Films films;

    public DetailFilm(Films films) {
        this.films = films;
    }

    public Films getFilms() {
        return films;
    }

    public void setFilms(Films films) {
        this.films = films;
    }
}
