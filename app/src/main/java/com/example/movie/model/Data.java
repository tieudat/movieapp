package com.example.movie.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {


    @SerializedName("data")
    private List<Films> mFilms;

    public Data(List<Films> mFilms) {
        this.mFilms = mFilms;
    }



    public List<Films> getmFilms() {
        return mFilms;
    }

    public void setmFilms(List<Films> mFilms) {
        this.mFilms = mFilms;
    }

    @Override
    public String toString() {
        return "Data{" +
                "mFilms=" + mFilms +
                '}';
    }
}
