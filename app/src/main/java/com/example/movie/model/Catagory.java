package com.example.movie.model;


public class Catagory {
    private int id;
    private String name;
    private int isActive;
    private int type;

    public Catagory() {
    }

    public Catagory(int id, String name, int isActive, int type) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Catagory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isActive=" + isActive +
                ", type=" + type +
                '}';
    }
}
