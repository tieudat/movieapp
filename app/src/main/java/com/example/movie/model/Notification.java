package com.example.movie.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Notification implements Serializable, Comparable<Notification> {
    private int id;
    private String title;
    private String description;
    private String linkImg;
    private int status;
    private LocalDateTime date;

    public Notification() {
    }

    public Notification(String title, String description, String linkImg, int status, LocalDateTime localDateTime) {
        this.title = title;
        this.description = description;
        this.linkImg = linkImg;
        this.status = status;
        this.date = localDateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLinkImg() {
        return linkImg;
    }

    public void setLinkImg(String linkImg) {
        this.linkImg = linkImg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", linkImg='" + linkImg + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public int compareTo(Notification o) {
        if (date == o.getDate()){
            return 0;
        }else if (date.isAfter(o.getDate())){
            return -1;
        }else {
            return 1;
        }
    }
}
