package com.example.movie.model;

import java.io.Serializable;

public class SubVideo implements Comparable<SubVideo>, Serializable {
    private int id;
    private int videoId;
    private int isActive;
    private int episode;
    private String link;
    private String redirectLink;

    public SubVideo() {
    }

    public SubVideo(int id, int videoId, int isActive, int episode, String link, String redirectLink) {
        this.id = id;
        this.videoId = videoId;
        this.isActive = isActive;
        this.episode = episode;
        this.link = link;
        this.redirectLink = redirectLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getEpisode() {
        return episode;
    }

    public void setEpisode(int episode) {
        this.episode = episode;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getRedirectLink() {
        return redirectLink;
    }

    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }

    @Override
    public int compareTo(SubVideo o) {
        if (episode == o.episode){
            return 0;
        }else if (episode > o.episode){
            return  1;
        }else {
            return -1;
        }
    }
}
