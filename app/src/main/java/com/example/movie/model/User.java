package com.example.movie.model;

public class User {
    private String id_token;
    private String userName;
    private String photoUrl;
    private String email;

    public User(String id_token, String userName, String photoUrl, String email) {
        this.id_token = id_token;
        this.userName = userName;
        this.photoUrl = photoUrl;
        this.email = email;
    }

    public String getId_token() {
        return id_token;
    }

    public void setId_token(String id_token) {
        this.id_token = id_token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
