package com.example.movie.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilmCatagoryList {
    @SerializedName("data")
    private List<Catagory> mListCatagory;

    public FilmCatagoryList(List<Catagory> mListCatagory) {
        this.mListCatagory = mListCatagory;
    }

    public List<Catagory> getmListCatagory() {
        return mListCatagory;
    }

    public void setmListCatagory(List<Catagory> mListCatagory) {
        this.mListCatagory = mListCatagory;
    }
}
