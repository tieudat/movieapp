package com.example.movie.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class Watched implements Serializable,Comparable<Watched> {
    private int id;
    private String name;
    private String avatar;
    private String link;
//    private long duration;
    private int viewNumber;
    private LocalDateTime date;
    private String redirectLink;

    public Watched(){

    };

    public Watched(String name, String avatar, String link, int viewNumber, LocalDateTime date, String redirectLink) {
        this.name = name;
        this.avatar = avatar;
        this.link = link;
        this.viewNumber = viewNumber;
        this.date = date;
        this.redirectLink = redirectLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getViewNumber() {
        return viewNumber;
    }

    public void setViewNumber(int viewNumber) {
        this.viewNumber = viewNumber;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getRedirectLink() {
        return redirectLink;
    }

    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }

    @Override
    public String toString() {
        return "Watched{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", link='" + link + '\'' +
                ", viewNumber=" + viewNumber +
                ", date=" + date +
                '}';
    }

    @Override
    public int compareTo(Watched o) {
       if (date == o.getDate()){
           return 0;
       }else if (date.isAfter(o.getDate())){
           return -1;
       }else {
           return 1;
       }
    }
}
