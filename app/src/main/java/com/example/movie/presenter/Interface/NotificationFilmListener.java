package com.example.movie.presenter.Interface;

import com.example.movie.model.Notification;

import java.util.List;

public interface NotificationFilmListener {
    void setData(List<Notification> list);
}
