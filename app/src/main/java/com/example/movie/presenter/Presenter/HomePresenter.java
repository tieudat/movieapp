package com.example.movie.presenter.Presenter;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.movie.R;
import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.Catagory;
import com.example.movie.model.Data;
import com.example.movie.model.FilmCatagoryList;
import com.example.movie.model.Photo;
import com.example.movie.presenter.Interface.HomeListenter;
import com.example.movie.view.adapter.CatagorySlideAdapter;
import com.example.movie.view.fragment.HomeFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter {
    private HomeListenter mHomeListener;

    public HomePresenter(HomeListenter mHomeListener) {
        this.mHomeListener = mHomeListener;
    }

    public void getListBanner(){
        mHomeListener.showBanner(getListPhoto());
    }


    private List<Photo> getListPhoto(){
        List<Photo> list = new ArrayList<>();
        list.add(new Photo(R.drawable.ic_onboarding_1));
        list.add(new Photo(R.drawable.ic_onboarding_2));
        list.add(new Photo(R.drawable.ic_onboarding_3));
        list.add(new Photo(R.drawable.ic_onboarding_3));
        list.add(new Photo(R.drawable.ic_onboarding_3));

        return list;
    }

    public void callApi(){
        String msisdn ="";
        String wsToken = Contants.WS_TOKEN;
        ApiService.apiService.getHomeMovie(msisdn,wsToken).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {

                Data data = response.body();
                if (data !=null){
                    mHomeListener.showFilm(data.getmFilms());
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
            }
        });
    }

    public void getFilmCatagoryList(){
        String wsToke = Contants.WS_TOKEN;
        ApiService.apiService.getFilmCatagoryList(wsToke,wsToke).enqueue(new Callback<FilmCatagoryList>() {
            @Override
            public void onResponse(Call<FilmCatagoryList> call, Response<FilmCatagoryList> response) {
                FilmCatagoryList list = response.body();

                if (list !=null){
                    mHomeListener.showCatagory(list.getmListCatagory());
                }
            }

            @Override
            public void onFailure(Call<FilmCatagoryList> call, Throwable t) {

            }
        });
    }

}
