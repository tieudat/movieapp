package com.example.movie.presenter.Presenter;

import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.Data;
import com.example.movie.presenter.Interface.HotFilmListenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HotFilmPresenter {
    private HotFilmListenter mHotFilmListenter;

    public HotFilmPresenter(HotFilmListenter mHotFilmListenter) {
        this.mHotFilmListenter = mHotFilmListenter;
    }

    public void searchFilm(String keySearch){


        String wsToken = Contants.WS_TOKEN;
        ApiService.apiService.searchFilm(keySearch,wsToken).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                Data data = response.body();
                if (data !=null){
                    mHotFilmListenter.showHotFilm(data.getmFilms());
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

            }
        });
    }
}
