package com.example.movie.presenter.Presenter;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.movie.database.DatabaseHandler;
import com.example.movie.model.Films;
import com.example.movie.model.Watched;
import com.example.movie.presenter.Interface.WatchedFilmListener;
import com.example.movie.view.adapter.WatchedFilmAdapter;

import java.util.Collections;
import java.util.List;

public class WatchedFilmPresenter {
    private Context context;
    private WatchedFilmListener mWatchedFilmListener;

    public WatchedFilmPresenter(WatchedFilmListener mWatchedFilmListener, Context context){
        this.mWatchedFilmListener = mWatchedFilmListener;
        this.context = context;
    }

    public void setDataWatched(){
        DatabaseHandler db = new DatabaseHandler(context);
        List<Watched> mListWatched = db.getListWatched();
        Collections.sort(mListWatched);
        mWatchedFilmListener.setDataWatched(mListWatched);

    }
}
