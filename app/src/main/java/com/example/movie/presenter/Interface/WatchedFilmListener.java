package com.example.movie.presenter.Interface;

import com.example.movie.model.Watched;

import java.util.List;

public interface WatchedFilmListener {
    void gotoExoPlayer(Watched watched);
    void shareFilmWatched(Watched watched);
    void downloadFilmWatched(Watched watched);
    void loadData();
    void setDataWatched(List<Watched> list);
}
