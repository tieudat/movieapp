package com.example.movie.presenter.Presenter;

import com.example.movie.R;
import com.example.movie.model.OnBoardingItem;
import com.example.movie.presenter.Interface.WelcomePageListenter;

import java.util.ArrayList;
import java.util.List;

public class WelcomePagePrensenter {
    private WelcomePageListenter mWelcomePageListenter;
    private List<OnBoardingItem> mList;

    public WelcomePagePrensenter(WelcomePageListenter mWelcomePageListenter){
        this.mWelcomePageListenter = mWelcomePageListenter;
    }

    public List<OnBoardingItem> getData(){
        mList = new ArrayList<>();
        OnBoardingItem onBoardingItem = new OnBoardingItem(R.raw.onboarding_1,"Welcome to Mova", "The best movie streaming app of the century to make your days great!");
        OnBoardingItem onBoardingItem1 = new OnBoardingItem(R.raw.onboarding_2,"Welcome to Mova", "The best movie streaming app of the century to make your days great!");
        OnBoardingItem onBoardingItem2 = new OnBoardingItem(R.raw.onboarding_3,"Welcome to Mova", "The best movie streaming app of the century to make your days great!");
        mList.add(onBoardingItem);
        mList.add(onBoardingItem1);
        mList.add(onBoardingItem2);
        return mList;
    }



}
