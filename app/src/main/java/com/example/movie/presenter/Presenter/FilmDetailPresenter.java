package com.example.movie.presenter.Presenter;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.DetailFilm;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.FilmDetailListenter;
import com.example.movie.view.ExoPlayVideo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilmDetailPresenter {
    private FilmDetailListenter mFilmDetailListenter;
    private Activity activity;
    private Films fi;

    public FilmDetailPresenter(FilmDetailListenter mFilmDetailListenter, Activity activity) {
        this.mFilmDetailListenter = mFilmDetailListenter;
        this.activity = activity;
    }

    public void getFilmDetail(){
        Bundle bundle = activity.getIntent().getExtras();
        fi = (Films) bundle.getSerializable("film_detail");
        String wsToken = Contants.WS_TOKEN;
        int id= fi.getId();
        ApiService.apiService.getFilmDetail(id,wsToken).enqueue(new Callback<DetailFilm>() {
            @Override
            public void onResponse(Call<DetailFilm> call, Response<DetailFilm> response) {
                DetailFilm detailFilm = response.body();
                if (detailFilm !=null){
                    Films films = detailFilm.getFilms();
                    mFilmDetailListenter.showDetail(films);
                }
            }
            @Override
            public void onFailure(Call<DetailFilm> call, Throwable t) {

            }
        });
    }

    public void downloadFilm(){
        mFilmDetailListenter.downloadFilm(fi);
    }

    public void shareFilm(){
        mFilmDetailListenter.shareFilm(fi);
    }
    public void gotoPlay(){
        mFilmDetailListenter.gotoExoPlayer(fi);
    }

}
