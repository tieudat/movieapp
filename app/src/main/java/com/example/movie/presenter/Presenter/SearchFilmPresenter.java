package com.example.movie.presenter.Presenter;

import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.Data;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.SearchFilmListenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFilmPresenter {
    private SearchFilmListenter mSearchFilmListenter;

    public SearchFilmPresenter(SearchFilmListenter mSearchFilmListenter) {
        this.mSearchFilmListenter = mSearchFilmListenter;
    }

    public void seachFilm(String keySearch){
        ApiService.apiService.searchFilm(keySearch, Contants.WS_TOKEN).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                Data data = response.body();
                if (data !=null){
                    List<Films> list = data.getmFilms();
                    mSearchFilmListenter.setDataSearchFilm(list);
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

            }
        });
    }
}
