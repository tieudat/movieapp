package com.example.movie.presenter.Interface;

import com.example.movie.model.Films;
import com.example.movie.model.SubVideo;

public interface FilmDetailListenter {
    void gotoExoPlayer(Films films);
    void gotoDetail(Films films);
    void showDetail(Films films);
    void downloadFilm(Films films);
    void toExoPlayer(Films films);
    void shareFilm(Films films);
}
