package com.example.movie.presenter.Interface;

import com.example.movie.model.Films;

import java.util.List;

public interface SearchFilmListenter {
    void setDataSearchFilm(List<Films> list);
    void gotoDetail(Films films);
}
