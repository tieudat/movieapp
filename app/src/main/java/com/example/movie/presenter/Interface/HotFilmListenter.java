package com.example.movie.presenter.Interface;

import com.example.movie.model.Films;

import java.util.List;

public interface HotFilmListenter {
    void showHotFilm(List<Films> list);
    void gotoDetail(Films films);
}
