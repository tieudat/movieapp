package com.example.movie.presenter.Interface;

import com.example.movie.model.Catagory;
import com.example.movie.model.Films;
import com.example.movie.model.Photo;

import java.util.List;

public interface HomeListenter {
    void goToDetail(Films films);
    void showFilm(List<Films> list);
    void showCatagory(List<Catagory> list);
    void showBanner(List<Photo> list);
}
