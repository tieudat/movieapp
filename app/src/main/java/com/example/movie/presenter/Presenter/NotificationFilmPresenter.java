package com.example.movie.presenter.Presenter;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.movie.database.DatabaseHandler;
import com.example.movie.presenter.Interface.NotificationFilmListener;
import com.example.movie.view.adapter.NotificationAdapter;

public class NotificationFilmPresenter {
    private NotificationFilmListener mNotificationFilmListener;
    private Context mContext;

    public NotificationFilmPresenter(NotificationFilmListener mNotificationFilmListener, Context mContext){
        this.mNotificationFilmListener = mNotificationFilmListener;
        this.mContext = mContext;
    }

    public void setDataNotification(){
        DatabaseHandler db = new DatabaseHandler(mContext);
        mNotificationFilmListener.setData(db.getListNotification());

    }

}
