package com.example.movie.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.movie.MyApplication;
import com.example.movie.R;
import com.example.movie.database.DatabaseHandler;
import com.example.movie.view.FilmDetail;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.time.LocalDateTime;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = MyFirebaseMessagingService.class.getName();

    @Override
    public void onMessageReceived(@NonNull RemoteMessage message) {
        super.onMessageReceived(message);
        RemoteMessage.Notification notification = message.getNotification();
        if (notification == null){
            return;
        }
        String strTitle = notification.getTitle();
        String strMessage = notification.getBody();
        LocalDateTime localDateTime = LocalDateTime.now();

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        com.example.movie.model.Notification noti= new com.example.movie.model.Notification();

        noti.setTitle(strTitle);
        noti.setDescription(strMessage);
        noti.setDate(localDateTime);

        db.addNotification(noti);

        sendNotification(strTitle,strMessage);
    }

    private void sendNotification(String strTitle,String strMessage){

        Intent intent = new Intent(this, FilmDetail.class);
        PendingIntent pe = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, MyApplication.CHANNEL_ID)
                .setContentTitle(strTitle)
                .setContentText(strMessage)
                .setSmallIcon(R.drawable.ic_logo_new)
                .setContentIntent(pe);

        Notification notification = notificationBuilder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager !=null){
            notificationManager.notify(1,notification);
        }
    }

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Log.e(TAG,token);
    }
}
