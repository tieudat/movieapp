package com.example.movie.custom_textview;

import android.content.Context;
import android.graphics.Typeface;

public class Utils {
    private static Typeface alexBrushTypeface;
    private static Typeface alluraTypeface;
    private static Typeface josefinSansTypeface;
    private static Typeface leagueSpartanTypeface;
    private static Typeface poppinsTypeface;

    public static Typeface getAlexBrushTypeface(Context context) {
        if (alexBrushTypeface == null){
            alexBrushTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/AlexBrush-Regular.ttf");
        }
        return alexBrushTypeface;
    }

    public static Typeface getAlluraTypeface(Context context) {
        if (alexBrushTypeface == null){
            alexBrushTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Allura-Regular.otf");
        }
        return alluraTypeface;
    }

    public static Typeface getJosefinSansTypeface(Context context) {
        if (josefinSansTypeface == null){
            josefinSansTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/JosefinSans-Bold.ttf");
        }
        return josefinSansTypeface;
    }

    public static Typeface getLeagueSpartanTypeface(Context context) {
        if (leagueSpartanTypeface == null){
            leagueSpartanTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/LeagueSpartan-Bold.otf");
        }
        return leagueSpartanTypeface;
    }

    public static Typeface getPoppinsTypeface(Context context) {
        if (poppinsTypeface == null){
            poppinsTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/Poppins-Bold.otf");
        }
        return poppinsTypeface;
    }
}
