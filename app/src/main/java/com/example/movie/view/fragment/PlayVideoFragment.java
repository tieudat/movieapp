package com.example.movie.view.fragment;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.view.FilmDetail;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.Tracks;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.StyledPlayerControlView;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;


public class PlayVideoFragment extends Fragment {

    private PlayerView playerView;
    private TextView tvTitle;
    private ImageView imgBack;

    private boolean flag = false;

    public PlayVideoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_play_video, container, false);
//        playerView = view.findViewById(R.id.video_player);
//        tvTitle = view.findViewById(R.id.video_title);
//        imgBack = view.findViewById(R.id.img_back_video);
//        imgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getContext(), FilmDetail.class);
//                startActivity(intent);
//            }
//        });
//
//
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//
//        Films fim = (Films)getArguments().getSerializable("film_play");
//        tvTitle.setText(fim.getName());
//
//        ExoPlayer player = new ExoPlayer.Builder(getContext()).build();
//        MediaItem mediaItem = MediaItem.fromUri(fim.getLink());
//        player.setMediaItem(mediaItem);
//        player.prepare();
//        player.play();
//        player.setPlayWhenReady(true);
//        playerView.setPlayer(player);



        return  view;
    }


}