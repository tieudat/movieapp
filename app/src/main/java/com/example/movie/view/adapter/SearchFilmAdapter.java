package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.SearchFilmListenter;

import java.util.List;

public class SearchFilmAdapter extends RecyclerView.Adapter<SearchFilmAdapter.SearchFilmViewHolder>{
    private List<Films> mListFilmSearch;
    private SearchFilmListenter mSearchFilmListenter;

    public void setData(List<Films> mListFilmSearch, SearchFilmListenter mSearchFilmListenter){
        this.mListFilmSearch   = mListFilmSearch;
        this.mSearchFilmListenter = mSearchFilmListenter;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchFilmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_film,parent,false);
        return new SearchFilmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchFilmViewHolder holder, int position) {
        Films films = mListFilmSearch.get(position);
        Glide.with(holder.imgSearchFilm).load(films.getAvatar()).placeholder(R.drawable.ic_default).into(holder.imgSearchFilm);
        holder.tvImdbFilm.setText(String.valueOf(films.getImdbPoInteger()));

        holder.imgSearchFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchFilmListenter.gotoDetail(films);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListFilmSearch !=null){
            return mListFilmSearch.size();
        }
        return 0;
    }

    public class SearchFilmViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgSearchFilm;
        private TextView tvImdbFilm;

        public SearchFilmViewHolder(@NonNull View itemView) {
            super(itemView);

            imgSearchFilm = itemView.findViewById(R.id.img_search_film);
            tvImdbFilm = itemView.findViewById(R.id.tv_icdb_search_film);
        }
    }
}
