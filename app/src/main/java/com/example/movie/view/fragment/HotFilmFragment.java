package com.example.movie.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.movie.R;
import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.Catagory;
import com.example.movie.model.Data;
import com.example.movie.model.Films;
import com.example.movie.model.Photo;
import com.example.movie.presenter.Interface.HomeListenter;
import com.example.movie.presenter.Interface.HotFilmListenter;
import com.example.movie.presenter.Presenter.HotFilmPresenter;
import com.example.movie.view.FilmDetail;
import com.example.movie.view.SearchFilm;
import com.example.movie.view.adapter.HotFilmAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HotFilmFragment extends Fragment implements HotFilmListenter, SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView recyclerView;
    private EditText edtSearchFilm;
    private List<Films> mListFilms;
    private HotFilmAdapter mHotFilmAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private HotFilmPresenter mHotFilmPresenter;
    private String keySearch ="";
    private ImageView imgSearch;


    public HotFilmFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hot_film,container,false);

        recyclerView = view.findViewById(R.id.rcv_hot_film);
        swipeRefreshLayout = view.findViewById(R.id.hot_film_layout);
        imgSearch = view.findViewById(R.id.img_search_film);

        mHotFilmPresenter.searchFilm(keySearch);

        swipeRefreshLayout.setOnRefreshListener(this);

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchFilm.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHotFilmPresenter = new HotFilmPresenter(this);
    }

    @Override
    public void onRefresh() {
        mHotFilmPresenter.searchFilm(keySearch);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        },2000);
    }

    @Override
    public void showHotFilm(List<Films> list) {
        mListFilms = list;
        mHotFilmAdapter = new HotFilmAdapter();
        mHotFilmAdapter.setData(mListFilms,HotFilmFragment.this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mHotFilmAdapter);
    }

    @Override
    public void gotoDetail(Films films) {
        Intent intent = new Intent(getContext(), FilmDetail.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("film_detail", films);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}