package com.example.movie.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.movie.R;
import com.example.movie.api.ApiService;
import com.example.movie.model.Catagory;
import com.example.movie.api.Contants;
import com.example.movie.model.Data;
import com.example.movie.model.FilmCatagoryList;
import com.example.movie.model.Films;
import com.example.movie.model.Photo;
import com.example.movie.presenter.Interface.HomeListenter;
import com.example.movie.presenter.Presenter.HomePresenter;
import com.example.movie.view.FilmDetail;
import com.example.movie.view.adapter.CatagorySlideAdapter;
import com.example.movie.view.adapter.PhotoViewAdapter;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator3;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements HomeListenter,SwipeRefreshLayout.OnRefreshListener  {

    private ViewPager2 mViewPager;
    private CircleIndicator3 mCircleIndicator;
    private List<Photo> mListPhoto;
    private List<Films> mListFilm;
    private List<Catagory> mListCatagory, mCatagory;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private PhotoViewAdapter photoViewAdapter;
    private RecyclerView rcvListFilm;
    private HomePresenter mHomePresenter;




    private Handler mHandler = new Handler();
    private Runnable mRunable = new Runnable() {
        @Override
        public void run() {
            if (mViewPager.getCurrentItem() == mListPhoto.size() - 1 ){
                mViewPager.setCurrentItem(0);
            }else {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);

            }
        }
    };

    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        callApi();
//        getFilmCatagoryList();

        mHomePresenter = new HomePresenter(this);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home,container,false);
        mViewPager = view.findViewById(R.id.view_pager_banner);
        mCircleIndicator = view.findViewById(R.id.circle_indicator);
        rcvListFilm = view.findViewById(R.id.rcv_list_film);
        mSwipeRefreshLayout = view.findViewById(R.id.home_layout);

        mHomePresenter.getListBanner();
        mHomePresenter.callApi();
        mHomePresenter.getFilmCatagoryList();

        mSwipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

//    private List<Photo> getListPhoto(){
//        List<Photo> list = new ArrayList<>();
//        list.add(new Photo(R.drawable.ic_onboarding_1));
//        list.add(new Photo(R.drawable.ic_onboarding_2));
//        list.add(new Photo(R.drawable.ic_onboarding_3));
//        list.add(new Photo(R.drawable.ic_onboarding_3));
//        list.add(new Photo(R.drawable.ic_onboarding_3));
//
//        return list;
//    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunable);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.postDelayed(mRunable,3000);
    }

    public void callApi(){
        String msisdn ="";
        String wsToken = Contants.WS_TOKEN;
        ApiService.apiService.getHomeMovie(msisdn,wsToken).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
//                Toast.makeText(getContext(),"Call Done",Toast.LENGTH_SHORT).show();

                Data data = response.body();
                if (data !=null){
                    mListFilm = new ArrayList<>();
                    mListFilm = data.getmFilms();

                    CatagorySlideAdapter catagorySlideAdapter  = new CatagorySlideAdapter();
                    catagorySlideAdapter.setData(mListFilm, mListCatagory,HomeFragment.this);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rcvListFilm.setLayoutManager(linearLayoutManager);
                    rcvListFilm.setAdapter(catagorySlideAdapter);
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
//                Toast.makeText(getContext(),"Call Fail",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getFilmCatagoryList(){
        String wsToke = Contants.WS_TOKEN;
        ApiService.apiService.getFilmCatagoryList(wsToke,wsToke).enqueue(new Callback<FilmCatagoryList>() {
            @Override
            public void onResponse(Call<FilmCatagoryList> call, Response<FilmCatagoryList> response) {
//                Toast.makeText(getContext(),"Call Success Catagory",Toast.LENGTH_SHORT).show();
                    FilmCatagoryList list = response.body();
                    mListCatagory = new ArrayList<>();
                    List<Catagory> listCatagory = list.getmListCatagory();
//                    Catagory b = new Catagory();
                    for (Catagory a : listCatagory){
                        if (a.getIsActive() ==1){
                            mListCatagory.add(a);
                        }
                    }
            }

            @Override
            public void onFailure(Call<FilmCatagoryList> call, Throwable t) {
//                Toast.makeText(getContext(),"Call Fail Catagory",Toast.LENGTH_SHORT).show();

            }
        });
    }





    @Override
    public void goToDetail(Films films) {
        Intent intent = new Intent(getContext(), FilmDetail.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("film_detail", films);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void showFilm(List<Films> list) {
        mListFilm = list;
        CatagorySlideAdapter catagorySlideAdapter  = new CatagorySlideAdapter();
        catagorySlideAdapter.setData(mListFilm, mListCatagory,HomeFragment.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvListFilm.setLayoutManager(linearLayoutManager);
        rcvListFilm.setAdapter(catagorySlideAdapter);

    }

    @Override
    public void showCatagory(List<Catagory> list) {
        mListCatagory = new ArrayList<>();
        List<Catagory> listCatagory = list;
        for (Catagory a : listCatagory){
            if (a.getIsActive() ==1){
                mListCatagory.add(a);
            }
        }
    }

    @Override
    public void showBanner(List<Photo> list) {
        mListPhoto = list;
        photoViewAdapter = new PhotoViewAdapter(mListPhoto);
        mViewPager.setAdapter(photoViewAdapter);
        mCircleIndicator.setViewPager(mViewPager);
        photoViewAdapter.registerAdapterDataObserver(mCircleIndicator.getAdapterDataObserver());

        mViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mHandler.removeCallbacks(mRunable);
                mHandler.postDelayed(mRunable,3000);
            }
        });

    }


    @Override
    public void onRefresh() {
        //        callApi();
        mHomePresenter.callApi();
        mViewPager.setAdapter(photoViewAdapter);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        },2000);
    }
}