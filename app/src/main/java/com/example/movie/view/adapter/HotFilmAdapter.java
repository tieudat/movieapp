package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.HomeListenter;
import com.example.movie.presenter.Interface.HotFilmListenter;

import java.util.List;

public class HotFilmAdapter extends RecyclerView.Adapter<HotFilmAdapter.HotFilmViewHolder> {
    private List<Films>  mList;
    private HotFilmListenter mHotFilmListenter;

    public void setData(List<Films> list, HotFilmListenter mHotFilmListenter){
        this.mList = list;
        this.mHotFilmListenter = mHotFilmListenter;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HotFilmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hot_film,parent, false);

        return new HotFilmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HotFilmViewHolder holder, int position) {

        Films films = mList.get(position);

        if (films.getIsHot() ==1){
            Glide.with(holder.imgHotFilm).load(films.getAvatar()).placeholder(R.drawable.ic_default).into(holder.imgHotFilm);
            holder.tvImdbHotFilm.setText(String.valueOf(films.getImdbPoInteger()));
            holder.imgHotFilm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mHotFilmListenter.gotoDetail(films);
                }
            });
        }



    }

    @Override
    public int getItemCount() {
        if (mList !=null){
            return mList.size();
        }
        return 0;
    }

    public class HotFilmViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgHotFilm;
        private TextView tvImdbHotFilm;

        public HotFilmViewHolder(@NonNull View itemView) {
            super(itemView);

            imgHotFilm = itemView.findViewById(R.id.img_hot_film);
            tvImdbHotFilm = itemView.findViewById(R.id.tv_icdb_hot_film);
        }
    }
}
