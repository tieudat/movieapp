package com.example.movie.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.movie.R;
import com.example.movie.model.OnBoardingItem;
import com.example.movie.presenter.Interface.WelcomePageListenter;
import com.example.movie.presenter.Presenter.WelcomePagePrensenter;
import com.example.movie.view.adapter.OnBoardingAdapter;

import java.util.ArrayList;
import java.util.List;

public class WelcomePage extends AppCompatActivity implements WelcomePageListenter {
    private OnBoardingAdapter onBoardingAdapter;
    private LinearLayout linearLayout;
    private Button btnSkip;
    private ViewPager2 viewPager2;
    private WelcomePagePrensenter welcomePagePrensenter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        initUI();

        welcomePagePrensenter = new WelcomePagePrensenter(this);
        onBoardingAdapter = new OnBoardingAdapter(welcomePagePrensenter.getData());

        viewPager2.setAdapter(onBoardingAdapter);
        setUpOnBoardingIndicators();
        setCurrentOnBoardingIndicator(0);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentOnBoardingIndicator(position);
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager2.getCurrentItem() +1 < onBoardingAdapter.getItemCount()){
                    viewPager2.setCurrentItem(viewPager2.getCurrentItem() +1);
                }else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        });
    }

    private void setUpOnBoardingIndicators(){
        ImageView[] indicators = new ImageView[onBoardingAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(8,0,8,0);
        for(int i = 0; i<indicators.length; i++){
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.onboarding_inactive
            ));
            indicators[i].setLayoutParams(layoutParams);
            linearLayout.addView(indicators[i]);
        }
    }

    private void setCurrentOnBoardingIndicator( int index){
        int childCount = linearLayout.getChildCount();
        for (int i = 0; i<childCount; i++){
            ImageView imageView = (ImageView) linearLayout.getChildAt(i);
            if (i ==index){
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),R.drawable.onboarding_active)
                );
            }else {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),R.drawable.onboarding_inactive)
                );
            }
        }
        if (index == onBoardingAdapter.getItemCount() -1){
            btnSkip.setText("Start");
        }else {
            btnSkip.setText("Next");
        }
    }

    @Override
    public void getData() {

    }
    private void initUI(){
        viewPager2 = findViewById(R.id.view_pager_onboarding);
        linearLayout= findViewById(R.id.layoutOnboardingIndicators);
        btnSkip = findViewById(R.id.btn_skip_onboarding);
    }
}