package com.example.movie.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.example.movie.R;
import com.example.movie.database.DatabaseHandler;
import com.example.movie.model.Notification;
import com.example.movie.presenter.Interface.IListentnerReloadData;
import com.example.movie.presenter.Interface.NotificationFilmListener;
import com.example.movie.presenter.Presenter.NotificationFilmPresenter;
import com.example.movie.view.adapter.NotificationAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotificationFilm extends AppCompatActivity implements IListentnerReloadData,SwipeRefreshLayout.OnRefreshListener, NotificationFilmListener {

    private ImageView imgBack;
    private List<Notification> mListNotification;
    private NotificationAdapter notificationAdapter;
    private RecyclerView rcvNotification;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NotificationFilmPresenter mNotificationFilmPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_film);
        rcvNotification =findViewById(R.id.rcv_notification);
        imgBack = findViewById(R.id.img_back_notification);
        swipeRefreshLayout = findViewById(R.id.notification_film_layout);
        mNotificationFilmPresenter = new NotificationFilmPresenter(this,this);
        mNotificationFilmPresenter.setDataNotification();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

       swipeRefreshLayout.setOnRefreshListener(this);


    }

    @Override
    public void loadData() {
        mNotificationFilmPresenter.setDataNotification();
    }

    @Override
    public void onRefresh() {
        mNotificationFilmPresenter.setDataNotification();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        },3000);
    }

    @Override
    public void setData(List<Notification> list) {
        Collections.sort(list);
        notificationAdapter = new NotificationAdapter();
        notificationAdapter.setData(list,this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcvNotification.setLayoutManager(linearLayoutManager);
        rcvNotification.setAdapter(notificationAdapter);
    }
}