package com.example.movie.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.SearchFilmListenter;
import com.example.movie.presenter.Presenter.SearchFilmPresenter;
import com.example.movie.view.adapter.SearchFilmAdapter;

import java.util.List;

public class SearchFilm extends AppCompatActivity implements SearchFilmListenter,SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rcvItemSearchFilm;
    private EditText edtSeachFilm;
    private SearchFilmPresenter mSearchFilmPresenter;
    private SearchFilmAdapter mSearchFilmAdapter;
    private LinearLayout layout;
    private ImageView imgBack;
    private SwipeRefreshLayout layoutSwipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_film);
        initUI();
        mSearchFilmPresenter = new SearchFilmPresenter(this);
        edtSeachFilm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String keySearch = edtSeachFilm.getText().toString().trim();
                mSearchFilmPresenter.seachFilm(keySearch);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layoutSwipe.setOnRefreshListener(this);

    }

    private void initUI(){
        rcvItemSearchFilm = findViewById(R.id.rcv_search_film);
        edtSeachFilm = findViewById(R.id.edt_search_film);
        layout = findViewById(R.id.layout_404_not_found);
        imgBack = findViewById(R.id.img_back_search_film);
        layoutSwipe = findViewById(R.id.layout_search_film);
    }

    @Override
    public void setDataSearchFilm(List<Films> list) {
        if(!list.isEmpty()){
            rcvItemSearchFilm.setVisibility(View.VISIBLE);
            mSearchFilmAdapter = new SearchFilmAdapter();
            mSearchFilmAdapter.setData(list,this);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2,RecyclerView.VERTICAL,false);
            rcvItemSearchFilm.setLayoutManager(gridLayoutManager);
            rcvItemSearchFilm.setAdapter(mSearchFilmAdapter);
            layout.setVisibility(View.GONE);
        }else {
            rcvItemSearchFilm.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void gotoDetail(Films films) {
        Intent intent = new Intent(SearchFilm.this, FilmDetail.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("film_detail", films);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutSwipe.setRefreshing(false);
            }
        },2000);
    }
}