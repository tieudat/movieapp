package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.model.SubVideo;
import com.example.movie.presenter.Interface.FilmDetailListenter;

import java.util.List;

public class VideoTrailerAdapter extends RecyclerView.Adapter<VideoTrailerAdapter.VideoTrailerViewHolder> {

    private Films films;
    private List<SubVideo> listSubVideo;
    private FilmDetailListenter filmDetailListenter;

    public void setData(List<SubVideo> listSubVideo, FilmDetailListenter filmDetailListenter,Films films){
        this.listSubVideo = listSubVideo;
        this.filmDetailListenter = filmDetailListenter;
        this.films = films;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VideoTrailerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trailer_film,parent,false);
        return new VideoTrailerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoTrailerViewHolder holder, int position) {

        SubVideo subVideo = listSubVideo.get(position);

        Glide.with(holder.imgVideoTrailer).load(films.getAvatar()).into(holder.imgVideoTrailer);
        holder.tvTitleTrailer.setText(films.getName() +" - Tập: " + subVideo.getEpisode());

        holder.imgVideoTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                films.setName(films.getName() +" - Tập: "+ subVideo.getEpisode());
                films.setLink(subVideo.getLink());
                filmDetailListenter.toExoPlayer(films);
            }
        });

        holder.tvTitleTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                films.setName(films.getName() +"- Tập: "+ subVideo.getEpisode());
                films.setLink(subVideo.getLink());
                filmDetailListenter.toExoPlayer(films);
            }
        });


    }

    @Override
    public int getItemCount() {
        if (listSubVideo !=null){
            return listSubVideo.size();
        }
        return 0;
    }

    public class VideoTrailerViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgVideoTrailer;
        private TextView tvTitleTrailer;

        public VideoTrailerViewHolder(@NonNull View itemView) {
            super(itemView);

            imgVideoTrailer = itemView.findViewById(R.id.img_video_trailer);
            tvTitleTrailer = itemView.findViewById(R.id.tv_trailer);

        }
    }
}
