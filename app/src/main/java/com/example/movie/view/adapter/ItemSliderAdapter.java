package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.HomeListenter;

import java.util.List;

public class ItemSliderAdapter extends RecyclerView.Adapter<ItemSliderAdapter.ItemSliderViewHolder> {
    private List<Films> mListFilm;
    private View mView;
    private HomeListenter homeListenter;

    public void setData(List<Films> list, HomeListenter homeListenter){
        this.mListFilm = list;
        this.homeListenter = homeListenter;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ItemSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
         mView = inflater.inflate(R.layout.item_slide_film, parent,false);

        return new ItemSliderViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemSliderViewHolder holder, int position) {

        Films films = mListFilm.get(position);
        if (films ==null){
            return;
        }
        Glide.with(holder.imgSlider).load(films.getAvatar()).placeholder(R.drawable.ic_default).into(holder.imgSlider);
        holder.imgSlider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeListenter.goToDetail(films);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mListFilm !=null){
            return mListFilm.size();
        }
        return 0;
    }

    public class ItemSliderViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgSlider;

        public ItemSliderViewHolder(@NonNull View itemView) {
            super(itemView);

            imgSlider = itemView.findViewById(R.id.img_slider);
        }
    }
}
