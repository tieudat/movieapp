package com.example.movie.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;

import android.Manifest;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.DetailFilm;
import com.example.movie.model.Films;
import com.example.movie.model.SubVideo;
import com.example.movie.presenter.Interface.FilmDetailListenter;
import com.example.movie.presenter.Presenter.FilmDetailPresenter;
import com.example.movie.view.adapter.DetailPageAdater;
import com.example.movie.view.fragment.TrailerFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilmDetail extends AppCompatActivity implements FilmDetailListenter,SwipeRefreshLayout.OnRefreshListener {

    private TabLayout mTabLayout;
    private ViewPager2 mViewPager2;
    private DetailPageAdater mDetailPageAdater;
    private ImageView imgBack;
    private ImageView imgPoster;
    private Button btnGotoPlay,btnDownloadFilm;
    private ImageView imgShare;
    private ImageView imgSaveFilm;
    private SwipeRefreshLayout refreshLayout;
    private Films fi;
    private TextView tvTitleDetail,tvQualityDetail,tvWatchViewDetail,tvStarDetail,tvDescriptionDetail;
    private FilmDetailPresenter mFilmDetailPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);


        initUI();
        mFilmDetailPresenter = new FilmDetailPresenter(this,this);
        mFilmDetailPresenter.getFilmDetail();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnGotoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFilmDetailPresenter.gotoPlay();
            }
        });
        btnDownloadFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });
        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFilmDetailPresenter.shareFilm();
//                openBottomSheetDialog();
            }
        });
        imgSaveFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Saved Success !", Toast.LENGTH_SHORT).show();
            }
        });

        refreshLayout.setOnRefreshListener(this);
    }


    private void initUI(){
        imgBack = findViewById(R.id.img_back);
        imgPoster =findViewById(R.id.img_poster_detail);
        tvTitleDetail = findViewById(R.id.tv_title_detail);
        tvQualityDetail = findViewById(R.id.tv_quality_detail);
        tvWatchViewDetail = findViewById(R.id.tv_watch_view_detail);
        tvStarDetail = findViewById(R.id.tv_star_detail);
        tvDescriptionDetail = findViewById(R.id.tv_description_detail);
        btnGotoPlay = findViewById(R.id.btn_go_to_play_video);
        mTabLayout = findViewById(R.id.tab_detail);
        mViewPager2 = findViewById(R.id.page_detail);
        refreshLayout = findViewById(R.id.detail_layout);
        imgShare = findViewById(R.id.img_share_detail);
        imgSaveFilm = findViewById(R.id.img_save_film);
        btnDownloadFilm = findViewById(R.id.btn_download_film_detail);
    }

    @Override
    public void onRefresh() {
        mFilmDetailPresenter.getFilmDetail();
        mViewPager2.setAdapter(mDetailPageAdater);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(false);
            }
        },3000);
    }

    private void openBottomSheetDialog(){
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_share_film_detail,null);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this,R.style.bottom_sheet_dialog);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();


    }

    private void checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(permission,Contants.REQUEST_PERMISSION_CODE);
            }else {
                mFilmDetailPresenter.downloadFilm();
            }
        }else {
            mFilmDetailPresenter.downloadFilm();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Contants.REQUEST_PERMISSION_CODE) {
            if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"Permission Denied!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void gotoExoPlayer(Films films) {
        Bundle bundleFilm = new Bundle();
        bundleFilm.putSerializable("video_play", films);
        Intent intent = new Intent(this,ExoPlayVideo.class);
        intent.putExtras(bundleFilm);
        startActivity(intent);
    }

    @Override
    public void gotoDetail(Films films) {

    }

    @Override
    public void showDetail(Films films) {
        tvTitleDetail.setText(films.getName());
        tvDescriptionDetail.setText(films.getDescription());
        tvWatchViewDetail.setText(String.valueOf(films.getViewNumber()));
        tvStarDetail.setText(String.valueOf(films.getStar()));
        tvQualityDetail.setText(films.getQuality());
        Glide.with(imgPoster).load(films.getAvatar()).into(imgPoster);

        fi = films;
        mDetailPageAdater = new DetailPageAdater(this,fi);
        mViewPager2.setAdapter(mDetailPageAdater);
        new TabLayoutMediator(mTabLayout, mViewPager2, (tab, position) -> {
            switch (position){
                case 0:
                    tab.setText("Episodes");
                    break;
                case 1:
                    tab.setText("More Like This");
                    break;
                case 2:
                    tab.setText("Comment");
                    break;
            }
        }).attach();
    }

    @Override
    public void downloadFilm(Films films) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(films.getLink()));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
        request.setTitle("Download");
        request.setDescription("Download file...");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,String.valueOf(System.currentTimeMillis()));
        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        if(downloadManager !=null){
            downloadManager.enqueue(request);
        }
    }

    @Override
    public void toExoPlayer(Films films) {

    }

    @Override
    public void shareFilm(Films films) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT,films.getRedirectLink());
        intent.setType("text/plain");
        startActivity(intent);
    }

}