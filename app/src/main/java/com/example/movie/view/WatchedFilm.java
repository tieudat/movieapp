package com.example.movie.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.example.movie.R;
import com.example.movie.database.DatabaseHandler;
import com.example.movie.model.Watched;
import com.example.movie.presenter.Interface.WatchedFilmListener;
import com.example.movie.presenter.Presenter.WatchedFilmPresenter;
import com.example.movie.view.adapter.WatchedFilmAdapter;

import java.util.Collections;
import java.util.List;

public class WatchedFilm extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, WatchedFilmListener {

    private SwipeRefreshLayout mSwipeRefeshLayout;
    private WatchedFilmAdapter mWatchedFilmAdapter;
    private ImageView imgBack;
    private RecyclerView rcvWatched;
    private List<Watched> mListWatched;
    private WatchedFilmPresenter mWatchedFilmPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watched_film);
        initUI();
        mWatchedFilmPresenter = new WatchedFilmPresenter(this,this);
        mWatchedFilmPresenter.setDataWatched();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSwipeRefeshLayout.setOnRefreshListener(this);
    }

    private void initUI(){
        mSwipeRefeshLayout = findViewById(R.id.watched_film_layout);
        imgBack = findViewById(R.id.img_back_watched);
        rcvWatched = findViewById(R.id.rcv_watched_film);
    }

    @Override
    public void onRefresh() {
        mWatchedFilmPresenter.setDataWatched();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefeshLayout.setRefreshing(false);
            }
        },3000);

    }

    @Override
    public void gotoExoPlayer(Watched watched) {
        Intent intent = new Intent(getApplicationContext(),ExoPlayVideo.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("video_play_watched", watched);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void shareFilmWatched(Watched watched) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,watched.getRedirectLink());
        startActivity(intent);
    }

    @Override
    public void downloadFilmWatched(Watched watched) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(watched.getLink()));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
        request.setTitle("Download");
        request.setDescription("Download file...");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,String.valueOf(System.currentTimeMillis()));

        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        if(downloadManager !=null){
            downloadManager.enqueue(request);
        }
    }

    @Override
    public void loadData() {
        mWatchedFilmPresenter.setDataWatched();
    }

    @Override
    public void setDataWatched(List<Watched> list) {
        mWatchedFilmAdapter = new WatchedFilmAdapter();
        mWatchedFilmAdapter.setData(list,this,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcvWatched.setLayoutManager(linearLayoutManager);
        rcvWatched.setAdapter(mWatchedFilmAdapter);
    }
}