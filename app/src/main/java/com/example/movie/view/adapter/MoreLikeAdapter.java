package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.FilmDetailListenter;
import com.example.movie.presenter.Interface.HomeListenter;

import java.util.List;

public class MoreLikeAdapter extends RecyclerView.Adapter<MoreLikeAdapter.MoreLikeViewHolder> {

    private List<Films> mListFilm;
    private FilmDetailListenter mFilmDetailListenter;

    public void setData(List<Films> mListFilm, FilmDetailListenter mFilmDetailListenter){
        this.mListFilm = mListFilm;
        this.mFilmDetailListenter = mFilmDetailListenter;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MoreLikeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_more_like,parent,false);
        return new MoreLikeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoreLikeViewHolder holder, int position) {
        Films films = mListFilm.get(position);
        Glide.with(holder.imgMoreLikeAvatar).load(films.getAvatar()).placeholder(R.drawable.ic_default).into(holder.imgMoreLikeAvatar);
        holder.tvIcdb.setText(String.valueOf(films.getImdbPoInteger()));
        holder.tvTitleMoreLike.setText(films.getName());

        holder.imgMoreLikeAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFilmDetailListenter.gotoDetail(films);
            }
        });

        holder.tvTitleMoreLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFilmDetailListenter.gotoDetail(films);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mListFilm !=null){
            return mListFilm.size();
        }
        return 0;
    }

    public class MoreLikeViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgMoreLikeAvatar;
        private TextView tvIcdb, tvTitleMoreLike;


        public MoreLikeViewHolder(@NonNull View itemView) {

            super(itemView);
            imgMoreLikeAvatar = itemView.findViewById(R.id.img_more_like_avatar);
            tvIcdb = itemView.findViewById(R.id.tv_icdb);
            tvTitleMoreLike = itemView.findViewById(R.id.tv_title_more_like);

        }
    }
}
