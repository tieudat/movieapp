package com.example.movie.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movie.R;
import com.example.movie.database.DatabaseHandler;
import com.example.movie.model.Notification;
import com.example.movie.presenter.Interface.IListentnerReloadData;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private List<Notification> mListNotification;
    private Context context;
    private IListentnerReloadData iListentnerReloadData;

    public void setData(List<Notification> mListNotification,Context context, IListentnerReloadData iListentnerReloadData){
        this.mListNotification = mListNotification;
        this.context = context;
        this.iListentnerReloadData = iListentnerReloadData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_film,parent,false);
       return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        Notification notification = mListNotification.get(position);
        if (notification == null){
            return;
        }
        holder.tvTitleNotification.setText(notification.getTitle());
        holder.tvDescriptionNotification.setText(notification.getDescription());
        if (notification.getStatus() ==0){
            holder.layout.setBackground(context.getDrawable(R.drawable.custom_bg_notification_film));
            holder.tvTitleNotification.setTextColor(context.getResources().getColor(R.color.black));
            holder.tvDescriptionNotification.setTextColor(context.getResources().getColor(R.color.black));
            holder.imgMore.setBackground(context.getResources().getDrawable(R.drawable.custom_bg_imdb));
            holder.imgNotification.setBackground(context.getResources().getDrawable(R.drawable.custom_bg_imdb));
        }

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                PopupMenu popupMenu = new PopupMenu(context,view);
//                popupMenu.getMenuInflater().inflate(R.menu.menu_item_notification, popupMenu.getMenu());
//                popupMenu.show();
//                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem menuItem) {
//                        switch (menuItem.getItemId()){
//                            case R.id.action_read_notification:
//                                Toast.makeText(context,"Đã đánh dấu đã đọc", Toast.LENGTH_SHORT).show();
//                                notification.setStatus(1);
//                                db.updateNotification(notification);
//                                iListentnerReloadData.loadData();
//
//                                return true;
//                            case R.id.action_delete_notification:
//                                Toast.makeText(context,"Delete Success", Toast.LENGTH_SHORT).show();
//                                db.deleteNotification(notification.getId());
//                                iListentnerReloadData.loadData();
//                                return true;
//                            default:
//                                return false;
//                        }
//                    }
//                });
                v = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_dialog_notification,null);
                BottomSheetDialog bottomSheetDialog1 = new BottomSheetDialog(context,R.style.bottom_sheet_dialog);
                bottomSheetDialog1.setContentView(v);
                bottomSheetDialog1.show();

                bottomSheetDialog1.findViewById(R.id.tv_read_notification_film).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseHandler db = new DatabaseHandler(context);
                        Toast.makeText(context,"Đã đánh dấu đã đọc", Toast.LENGTH_SHORT).show();
                        notification.setStatus(1);
                        db.updateNotification(notification);
                        bottomSheetDialog1.cancel();
                        iListentnerReloadData.loadData();
                    }
                });

                bottomSheetDialog1.findViewById(R.id.tv_delete_notification).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseHandler db = new DatabaseHandler(context);
                        Toast.makeText(context,"Delete Success", Toast.LENGTH_SHORT).show();
                        db.deleteNotification(notification.getId());
                        bottomSheetDialog1.cancel();
                        iListentnerReloadData.loadData();
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mListNotification !=null){
            return mListNotification.size();
        }
        return 0;
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layout;
        private TextView tvTitleNotification, tvDescriptionNotification;
        private ImageView imgMore,imgNotification;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitleNotification = itemView.findViewById(R.id.tv_title_notification);
            tvDescriptionNotification = itemView.findViewById(R.id.tv_description_notification);
            layout = itemView.findViewById(R.id.layout_notification_background);
            imgMore = itemView.findViewById(R.id.img_more_notification);
            imgNotification = itemView.findViewById(R.id.img_notification);
        }

    }



}
