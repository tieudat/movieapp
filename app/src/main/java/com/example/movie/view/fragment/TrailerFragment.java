package com.example.movie.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.movie.R;
import com.example.movie.model.Films;
import com.example.movie.model.SubVideo;
import com.example.movie.presenter.Interface.FilmDetailListenter;
import com.example.movie.view.ExoPlayVideo;
import com.example.movie.view.FilmDetail;
import com.example.movie.view.adapter.VideoTrailerAdapter;

import java.util.Collections;
import java.util.List;


public class TrailerFragment extends Fragment implements FilmDetailListenter {

    private VideoTrailerAdapter mVideoTrailer;
    private RecyclerView rcvTrailer;
    private List<SubVideo> mListSubVideo;

    public TrailerFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Films films = (Films) getArguments().getSerializable("detail_film");
        mListSubVideo = films.getListSubVideo();
        if (mListSubVideo != null){
            Collections.sort(mListSubVideo);
        }

        View view = inflater.inflate(R.layout.fragment_trailer, container, false);
        rcvTrailer = view.findViewById(R.id.rcv_trailer_film);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mVideoTrailer = new VideoTrailerAdapter();
        mVideoTrailer.setData(mListSubVideo,TrailerFragment.this,films);
        rcvTrailer.setLayoutManager(linearLayoutManager);
        rcvTrailer.setAdapter(mVideoTrailer);


        return view;
    }
    @Override
    public void gotoExoPlayer(Films films) {

    }

    @Override
    public void gotoDetail(Films films) {

    }

    @Override
    public void showDetail(Films films) {

    }

    @Override
    public void downloadFilm(Films films) {

    }

    @Override
    public void toExoPlayer(Films film) {
        Intent intent = new Intent(getContext(), ExoPlayVideo.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("trailer_film",film);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void shareFilm(Films films) {

    }
}