package com.example.movie.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.database.DatabaseHandler;
import com.example.movie.model.Watched;
import com.example.movie.presenter.Interface.WatchedFilmListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

public class WatchedFilmAdapter extends RecyclerView.Adapter<WatchedFilmAdapter.WatchedFilmViewHolder> {

    private List<Watched> mListWatched;
    private Context context;
    private WatchedFilmListener watchedFilmListener;

    public void setData(List<Watched> mListWatched, Context context, WatchedFilmListener watchedFilmListener){
        this.mListWatched = mListWatched;
        this.context = context;
        this.watchedFilmListener = watchedFilmListener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WatchedFilmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_watch_film,parent,false);

        return new WatchedFilmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchedFilmViewHolder holder, int position) {
        Watched watched = mListWatched.get(position);
        holder.tvTitleFilm.setText(watched.getName());
        Glide.with(holder.imgFilm).load(watched.getAvatar()).placeholder(R.drawable.ic_default).into(holder.imgFilm);

        holder.imgFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                watchedFilmListener.gotoExoPlayer(watched);
            }
        });
        holder.tvViewNumber.setText(String.valueOf(watched.getViewNumber()));

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_dialog_watched,null);
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context,R.style.bottom_sheet_dialog);
                bottomSheetDialog.setContentView(v);
                bottomSheetDialog.show();
                bottomSheetDialog.findViewById(R.id.tv_delete_watched_film).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context,"Xoá phim đã xem thành công",Toast.LENGTH_SHORT).show();
                        DatabaseHandler db = new DatabaseHandler(context);
                        db.deleteWatched(watched.getId());
                        bottomSheetDialog.cancel();
                        watchedFilmListener.loadData();
                    }
                });
                bottomSheetDialog.findViewById(R.id.tv_download_video).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        watchedFilmListener.downloadFilmWatched(watched);
                        bottomSheetDialog.cancel();
                    }
                });
                bottomSheetDialog.findViewById(R.id.tv_share_video_watched).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        watchedFilmListener.shareFilmWatched(watched);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListWatched !=null){
            return mListWatched.size();
        }
        return 0;
    }

    public class WatchedFilmViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTitleFilm,tvViewNumber;
        private TextView tvDurationFilm;
        private ProgressBar progressBar;
        private ImageView imgFilm, imgMore;

        public WatchedFilmViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitleFilm = itemView.findViewById(R.id.tv_title_watched_film);
            tvViewNumber = itemView.findViewById(R.id.tv_view_number_watched_film);
//            tvDurationFilm = itemView.findViewById(R.id.tv_duration_watched_film);
//            progressBar = itemView.findViewById(R.id.pgb_watched_film);
            imgFilm = itemView.findViewById(R.id.img_avatar_watched_film);
            imgMore = itemView.findViewById(R.id.img_more_watched);
        }
    }

}
