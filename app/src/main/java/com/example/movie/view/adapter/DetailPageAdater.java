package com.example.movie.view.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.movie.model.Films;
import com.example.movie.view.fragment.CommentFragment;
import com.example.movie.view.fragment.MoreLikeFragment;
import com.example.movie.view.fragment.TrailerFragment;

public class DetailPageAdater extends FragmentStateAdapter {

    private TrailerFragment trailerFragment;
    private MoreLikeFragment moreLikeFragment;
    private CommentFragment commentFragment;
    private Films films;


    public DetailPageAdater(@NonNull FragmentActivity fragmentActivity, Films films) {
        super(fragmentActivity);
        this.films = films;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("detail_film",films);

        switch (position){
            case 0:
                trailerFragment = new TrailerFragment();
                trailerFragment.setArguments(bundle);
                return trailerFragment;
            case 1:
                moreLikeFragment = new MoreLikeFragment();
                return moreLikeFragment;
            case 2:
                commentFragment = new CommentFragment();
                return commentFragment;
            default:
                trailerFragment.setArguments(bundle);
                return trailerFragment;

        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
