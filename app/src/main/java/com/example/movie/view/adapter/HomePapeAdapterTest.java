package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.movie.R;
import com.example.movie.model.TabHomeModel;
import com.example.movie.presenter.Interface.HomeListenter;
import com.google.firebase.database.core.Context;

import java.util.List;

import me.relex.circleindicator.CircleIndicator3;

public class HomePapeAdapterTest extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final String TYPE_BOX_BANNER = "Banner";
    public static final String TYPE_BOX_SLIDE = "Slide";

    private static int TYPE_HOME_BANNER = 1;
    private static int TYPE_ITEM_SLIDE = 2;

    private HomeListenter mHomeListenter;
    private List<TabHomeModel> mListTabHomeModel;
    private Context context;

    public void setData(Context context,List<TabHomeModel> mListTabHomeModel, HomeListenter mHomeListenter){
        this.context = context;
        this.mListTabHomeModel = mListTabHomeModel;
        this.mHomeListenter = mHomeListenter;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (TYPE_HOME_BANNER == viewType){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner_home,parent,false);
            return new HomeBannerViewHolder(view);
        }else if (TYPE_ITEM_SLIDE == viewType){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_film_home,parent,false);
            return new ItemSlideViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TabHomeModel model = mListTabHomeModel.get(position);

        if (model == null){
            return;
        }

        if (TYPE_HOME_BANNER == holder.getItemViewType()){
            HomeBannerViewHolder homeBannerViewHolder = (HomeBannerViewHolder) holder;
//            PhotoViewAdapter photoViewAdapter = new PhotoViewAdapter(,model.getListPhoto());



        }else if (TYPE_ITEM_SLIDE == holder.getItemViewType()){
            ItemSlideViewHolder itemSlideViewHolder = (ItemSlideViewHolder) holder;


        }

    }

    @Override
    public int getItemCount() {
        if (mListTabHomeModel !=null){
            return mListTabHomeModel.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        TabHomeModel tabHomeModel = mListTabHomeModel.get(position);
        if (tabHomeModel != null){
            if (tabHomeModel.getType().equals(TYPE_BOX_BANNER) && !tabHomeModel.getListPhoto().isEmpty()){
                return TYPE_HOME_BANNER;
            }
            else if (tabHomeModel.getType().equals(TYPE_BOX_SLIDE) && !tabHomeModel.getListCatagory().isEmpty() ){
                return TYPE_ITEM_SLIDE;
            }
        }
        return 0;
    }

    public class HomeBannerViewHolder extends RecyclerView.ViewHolder{

        private ViewPager mViewPager;
        private CircleIndicator3 mCircleIndicator3;

        public HomeBannerViewHolder(@NonNull View itemView) {
            super(itemView);

            mViewPager = itemView.findViewById(R.id.view_pager_banner_test);
            mCircleIndicator3 = itemView.findViewById(R.id.circle_indicator_test);
        }
    }

    public class ItemSlideViewHolder extends RecyclerView.ViewHolder{
        private RecyclerView recyclerView;

        public ItemSlideViewHolder(@NonNull View itemView) {
            super(itemView);

            recyclerView = itemView.findViewById(R.id.rcv_list_film_test);
        }
    }
}
