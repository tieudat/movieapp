package com.example.movie.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movie.R;
import com.example.movie.view.AppInfo;
import com.example.movie.view.LoginActivity;
import com.example.movie.view.NotificationFilm;
import com.example.movie.view.WatchedFilm;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class SettingFragment extends Fragment {

    private TextView tvNotification, tvWatched,tvAppInfo,tvUserName,tvLogout;
    private Switch swAutoTheme;
    private ImageView imgAvatar;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    public SettingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        tvNotification = view.findViewById(R.id.tv_go_to_notification);
        tvWatched = view.findViewById(R.id.tv_watched_film);
        tvAppInfo = view.findViewById(R.id.tv_app_info);
        swAutoTheme = view.findViewById(R.id.sw_auto_theme);
        sharedPreferences = getContext().getSharedPreferences("light",Context.MODE_PRIVATE);
        tvUserName = view.findViewById(R.id.tv_user_name_login);
        imgAvatar = view.findViewById(R.id.img_avatar_login);
        tvLogout = view.findViewById(R.id.tv_log_out);




        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser !=null){
            tvUserName.setText(mFirebaseUser.getDisplayName());
            Glide.with(imgAvatar).load(mFirebaseUser.getPhotoUrl()).into(imgAvatar);

        }

        checkState();

        tvNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), NotificationFilm.class);
                startActivity(intent);
            }
        });
        tvWatched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), WatchedFilm.class);
                startActivity(intent);
            }
        });

        Display display = getActivity().getWindowManager().getDefaultDisplay();



        swAutoTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swAutoTheme.isChecked()){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    setState(true);
                }else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    setState(false);
                }
            }
        });

        tvAppInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AppInfo.class);
                startActivity(intent);
            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAuth.signOut();
                LoginManager.getInstance().logOut();
                openLogin();
            }
        });


        return view;
    }





    private void checkState(){
        if (sharedPreferences.getBoolean("night_mode",false)){
            swAutoTheme.setChecked(true);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else {
            swAutoTheme.setChecked(false);
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
    }

    private void setState(boolean state){
        editor = sharedPreferences.edit();
        editor.putBoolean("night_mode",state);
        editor.apply();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mFirebaseUser ==null){
            openLogin();
        }
    }

    private void openLogin(){
        startActivity(new Intent(getContext(), LoginActivity.class));
    }
}