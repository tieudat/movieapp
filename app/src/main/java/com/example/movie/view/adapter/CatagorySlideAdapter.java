package com.example.movie.view.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movie.R;
import com.example.movie.model.Catagory;
import com.example.movie.model.Films;
import com.example.movie.presenter.Interface.HomeListenter;

import java.util.ArrayList;
import java.util.List;

public class CatagorySlideAdapter extends RecyclerView.Adapter<CatagorySlideAdapter.CatagoryViewHolder> {

    private List<Films> mListFilm;
    private List<Films> mList;
    private List<Catagory> mListCatagory;
    private View mView;
    private HomeListenter mHomeListenter;



    public void setData(List<Films> mListFilm, List<Catagory> mListCatagory, HomeListenter mHomeListenter){
        this.mListFilm = mListFilm;
        this.mListCatagory = mListCatagory;
        this.mHomeListenter = mHomeListenter;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public CatagoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        mView = inflater.inflate(R.layout.item_catagory_slide,parent,false);

        return new CatagoryViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull CatagoryViewHolder holder, int position) {

        try {
            Catagory catagory = mListCatagory.get(position);
            holder.tvTitle.setText(catagory.getName());

            mList =new ArrayList<>();
            for (Films a: mListFilm){
                if (a.getCategoryId() == catagory.getId()){
                    mList.add(a);
                }
            }
            if (mList.size() ==0){
                holder.tvTitle.setVisibility(View.GONE);
            }


            ItemSliderAdapter itemSliderAdapter = new ItemSliderAdapter();
            itemSliderAdapter.setData(mList, mHomeListenter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mView.getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            holder.rcvImageSlide.setLayoutManager(linearLayoutManager);
            holder.rcvImageSlide.setAdapter(itemSliderAdapter);


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (mListCatagory !=null){
            return mListCatagory.size();
        }
        return 0;
    }

    public class CatagoryViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTitle;
        private RecyclerView rcvImageSlide;

        public CatagoryViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_title_catagory);
            rcvImageSlide = itemView.findViewById(R.id.rcv_slide_film);
        }
    }
}
