package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.example.movie.R;
import com.example.movie.model.OnBoardingItem;

import java.util.List;

public class OnBoardingAdapter extends RecyclerView.Adapter<OnBoardingAdapter.OnBoardingViewHolder> {
    private List<OnBoardingItem> mList;

    public OnBoardingAdapter(List<OnBoardingItem> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public OnBoardingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_container_onboarding,parent,false);
        return new OnBoardingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OnBoardingViewHolder holder, int position) {

        OnBoardingItem onBoardingItem = mList.get(position);
        if(onBoardingItem ==null){
            return;
        }

        holder.tvTitle.setText(onBoardingItem.getTitle());
        holder.tvDescription.setText(onBoardingItem.getDescription());
        holder.lottieOnboarding.setAnimation(onBoardingItem.getImg());

    }

    @Override
    public int getItemCount() {
        if (mList !=null){
            return mList.size();
        }
        return 0;
    }

    public class OnBoardingViewHolder extends RecyclerView.ViewHolder{

        private LottieAnimationView lottieOnboarding;
        private TextView tvTitle,tvDescription;

        public OnBoardingViewHolder(@NonNull View itemView) {
            super(itemView);

            lottieOnboarding = itemView.findViewById(R.id.lottieOnboarding);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDescription = itemView.findViewById(R.id.tv_description);
        }
    }
}
