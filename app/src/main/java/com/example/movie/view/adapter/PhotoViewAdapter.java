package com.example.movie.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.example.movie.R;
import com.example.movie.model.Photo;

import java.util.List;

public class PhotoViewAdapter extends RecyclerView.Adapter<PhotoViewAdapter.PhotoViewHolder> {

    private List<Photo> mListPhoto;

    public PhotoViewAdapter( List<Photo> mListPhoto) {
        this.mListPhoto = mListPhoto;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo,parent,false);

        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {

        Photo photo = mListPhoto.get(position);
        if(photo == null){
            return;
        }
        Glide.with(holder.imgPhoto).load(photo.getResourceId()).placeholder(R.drawable.ic_default_banner).into(holder.imgPhoto);

    }

    @Override
    public int getItemCount() {
        if (mListPhoto !=null){
            return mListPhoto.size();
        }
        return 0;
    }


    public class PhotoViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgPhoto;

        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);

            imgPhoto = itemView.findViewById(R.id.img_banner);
        }
    }


}
