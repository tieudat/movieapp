package com.example.movie.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.movie.R;
import com.example.movie.api.ApiService;
import com.example.movie.api.Contants;
import com.example.movie.model.Catagory;
import com.example.movie.model.Data;
import com.example.movie.model.Films;
import com.example.movie.model.Photo;
import com.example.movie.model.SubVideo;
import com.example.movie.presenter.Interface.FilmDetailListenter;
import com.example.movie.presenter.Interface.HomeListenter;
import com.example.movie.view.FilmDetail;
import com.example.movie.view.adapter.MoreLikeAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MoreLikeFragment extends Fragment implements FilmDetailListenter {


    private RecyclerView mRecyclerView;
    private List<Films> mListFilm;
    private MoreLikeAdapter moreLikeAdapter;


    public MoreLikeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getRelatedFilm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more_like,container,false);

        mRecyclerView = view.findViewById(R.id.rcv_more_like);


        return view;
    }


    private void getRelatedFilm(){
        Bundle bundle = getActivity().getIntent().getExtras();
        Films films = (Films) bundle.getSerializable("film_detail");

        String wsToken = Contants.WS_TOKEN;
        ApiService.apiService.getRelatedFilm(films.getCategoryId(), 0,10,wsToken).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
//                Toast.makeText(getContext(),"Call Done More",Toast.LENGTH_SHORT).show();
                Data data = response.body();
                mListFilm = data.getmFilms();
                if (mListFilm != null){
                    try {
                        moreLikeAdapter = new MoreLikeAdapter();
                        moreLikeAdapter.setData(mListFilm,MoreLikeFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                        mRecyclerView.setLayoutManager(linearLayoutManager);
                        mRecyclerView.setAdapter(moreLikeAdapter);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
//                Toast.makeText(getContext(),"Call Fail More",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void gotoExoPlayer(Films films) {

    }

    @Override
    public void gotoDetail(Films films) {
        Intent intent = new Intent(getContext(), FilmDetail.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("film_detail", films);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void showDetail(Films films) {

    }

    @Override
    public void downloadFilm(Films films) {

    }

    @Override
    public void toExoPlayer(Films films) {

    }

    @Override
    public void shareFilm(Films films) {

    }
}