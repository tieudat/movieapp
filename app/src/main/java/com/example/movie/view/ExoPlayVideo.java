package com.example.movie.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.movie.R;
import com.example.movie.database.DatabaseHandler;
import com.example.movie.model.Films;
import com.example.movie.model.SubVideo;
import com.example.movie.model.Watched;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ExoPlayVideo extends AppCompatActivity {

    private PlayerView playerView;
    private TextView tvTitle;
    private ImageView imgBack;
    private ExoPlayer player;
    private ImageView imgFullScreen;
    private ProgressBar progressBar;
    private MediaItem mediaItem;
    private LinearLayout layout,layoutRewind,layoutForward;

    private boolean flag = false;
    private boolean intLeft,intRight;

    private boolean isEnable = true;
    private Point size;
    private int sWidth;
    private Display display;
    private GestureDetectorCompat gestureDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView(R.layout.activity_exo_play_video);
        initUI();
        playFilm();
        playError();
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        sWidth = size.x;
        gestureDetector = new GestureDetectorCompat(getApplicationContext(),new GestureDetector());
        addWatchedVideo();
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFullScreenView();
            }
        });

        playerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                gestureDetector.onTouchEvent(event);

                return true;
            }
        });

    }
    private void initUI(){
        playerView = findViewById(R.id.video_player);
        tvTitle = findViewById(R.id.video_title);
        imgBack = findViewById(R.id.img_back_video);
        imgFullScreen = findViewById(R.id.img_full_screen);
        progressBar = findViewById(R.id.progress_bar);
        layout = findViewById(R.id.play_video_layout);
        layoutRewind = findViewById(R.id.layout_rewind);
        layoutForward = findViewById(R.id.layout_forward);
    }

    private void playFilm(){
        Bundle bundle = getIntent().getExtras();
        Films filmsTrailer = (Films) bundle.getSerializable("trailer_film");
        Watched watched = (Watched) bundle.getSerializable("video_play_watched");
        Films fim = (Films)bundle.getSerializable("video_play")  ;
        player = new ExoPlayer.Builder(getApplicationContext()).build();

        if (watched !=null) {
            tvTitle.setText(watched.getName());
            mediaItem = MediaItem.fromUri(watched.getLink());
        }else if (fim !=null){
            tvTitle.setText(fim.getName());
            mediaItem = MediaItem.fromUri(fim.getLink());
        }else if (filmsTrailer !=null){
            tvTitle.setText(filmsTrailer.getName());
            mediaItem = MediaItem.fromUri(filmsTrailer.getLink());
        }

        player.setMediaItem(mediaItem);
        player.prepare();
        player.play();
        player.setPlayWhenReady(true);
        playerView.setPlayer(player);
        playerView.setKeepScreenOn(true);
        loadVideo();

    }
    private void playError(){
        player.addListener(new Player.Listener() {
            @Override
            public void onPlayerError(PlaybackException error) {
                Player.Listener.super.onPlayerError(error);
                Toast.makeText(ExoPlayVideo.this,"Video Playing Error", Toast.LENGTH_SHORT).show();

            }
        });
        player.setPlayWhenReady(true);
    }


    private void loadVideo(){
        player.addListener(new Player.Listener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Player.Listener.super.onPlayerStateChanged(playWhenReady, playbackState);

                if (playbackState == Player.STATE_BUFFERING){
                    progressBar.setVisibility(View.VISIBLE);
                }else {
                    progressBar.setVisibility(View.GONE);
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (player.isPlaying()){
            player.stop();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.setPlayWhenReady(false);
        player.getPlaybackState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        player.setPlayWhenReady(true);
        player.getPlaybackState();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        player.setPlayWhenReady(true);
        player.getPlaybackState();
    }
    private void setFullScreen(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void setFullScreenView(){
        if (flag){
            imgFullScreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen));
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            flag = false;
        }else {
            imgFullScreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen_exit));
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            flag = true;
        }
    }
    private void addWatchedVideo() {
        LocalDateTime localTime = LocalDateTime.now();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        Bundle bundle = getIntent().getExtras();
        Films film = (Films)bundle.getSerializable("video_play");
        Films filmTrailer = (Films) bundle.getSerializable("trailer_film");
        Watched filmWatched = (Watched) bundle.getSerializable("video_play_watched");
        Watched watched = new Watched();

        if (filmWatched !=null){
            filmWatched.setDate(localTime);
            db.updateWatched(filmWatched);
        }else if (film != null){
            watched.setName(film.getName());
            watched.setAvatar(film.getAvatar());
            watched.setLink(film.getLink());
            watched.setViewNumber(film.getViewNumber());
            watched.setDate(localTime);
            watched.setRedirectLink(film.getRedirectLink());

            if (db.getNameWatched(film.getName())!=null && db.getNameWatched(film.getName()).getName().equals(film.getName())){
                db.updateWatched(watched);
            }else {
                db.addWatched(watched);
            }

        }else if (filmTrailer !=null){
            watched.setName(filmTrailer.getName());
            watched.setAvatar(filmTrailer.getAvatar());
            watched.setLink(filmTrailer.getLink());
            watched.setViewNumber(filmTrailer.getViewNumber());
            watched.setDate(localTime);
            watched.setRedirectLink(filmTrailer.getRedirectLink());

            if (db.getNameWatched(filmTrailer.getName())!=null && db.getNameWatched(filmTrailer.getName()).getName().equals(filmTrailer.getName())){
                db.updateWatched(watched);
            }else {
                db.addWatched(watched);
            }

        }



    }

    private class GestureDetector extends android.view.GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if(isEnable){
                isEnable = false;
                playerView.hideController();
            }else {
                isEnable = true;
                playerView.showController();
            }


            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent event) {

            if (event.getX() < (sWidth / 2)){
                intLeft = true;
                intRight = false;
                player.seekTo(player.getCurrentPosition() - 10000);
                layoutRewind.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layoutRewind.setVisibility(View.INVISIBLE);
                    }
                },2000);
            }else if (event.getX() > (sWidth / 2)){
                intLeft = false;
                intRight = true;
                player.seekTo(player.getCurrentPosition() + 10000);
                layoutForward.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layoutForward.setVisibility(View.INVISIBLE);
                    }
                },2000);

            }


            return super.onDoubleTap(event);
        }
    }



}