package com.example.movie.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.movie.MainActivity;
import com.example.movie.R;

public class LoginWithAccount extends AppCompatActivity {


    private Button btnSignin;
    private TextView tvSingup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_account);

        btnSignin = findViewById(R.id.btn_sign_in);
        tvSingup = findViewById(R.id.tv_sign_up);

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginWithAccount.this, MainActivity.class));
            }
        });
        tvSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginWithAccount.this,CreateAccount.class));
            }
        });
    }
}